package com.phoenix.springbackend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// @MapperScan 扫包肯定能扫到的
@SpringBootApplication
@MapperScan("com.phoenix.springbackend.mapper")
public class SpringBackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBackendApplication.class, args);
    }
}

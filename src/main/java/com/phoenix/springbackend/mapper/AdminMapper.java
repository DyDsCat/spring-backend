package com.phoenix.springbackend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.phoenix.springbackend.entity.Admin;

public interface AdminMapper extends BaseMapper<Admin> {

}

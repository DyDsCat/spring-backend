package com.phoenix.springbackend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.phoenix.springbackend.entity.User;
import org.springframework.stereotype.Repository;


public interface UserMapper extends BaseMapper<User> {

}

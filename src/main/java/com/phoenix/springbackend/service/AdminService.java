package com.phoenix.springbackend.service;

import com.phoenix.springbackend.entity.ModelParams;

public interface AdminService {

    String test();

    void modelToggle();

    void modelTrain(ModelParams modelParams);

    void SaveModelParams(ModelParams modelParams);
}

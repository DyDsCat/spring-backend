package com.phoenix.springbackend.service.serviceImpl;

import com.phoenix.springbackend.entity.ModelParams;
import com.phoenix.springbackend.mapper.AdminMapper;
import com.phoenix.springbackend.service.AdminService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AdminServiceImpl implements AdminService {
    @Resource
    private AdminMapper adminMapper;


    @Override
    public String test() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("http://127.0.0.1:5000/test", String.class);
    }

    @Override
    public void modelToggle() {

    }

    @Override
    public void modelTrain(ModelParams modelParams) {

    }

    @Override
    public void SaveModelParams(ModelParams modelParams) {

    }
}

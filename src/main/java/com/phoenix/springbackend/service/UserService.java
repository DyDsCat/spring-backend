package com.phoenix.springbackend.service;

import com.phoenix.springbackend.entity.User;

import java.io.File;

public interface UserService {
    User login(User user);

    User register(User user);

    String digitRecognition(File file);
}

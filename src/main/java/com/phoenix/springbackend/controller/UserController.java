package com.phoenix.springbackend.controller;

import com.phoenix.springbackend.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@RestController
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("/user/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        RestTemplate restTemplate = new RestTemplate();

        ByteArrayResource resource = new ByteArrayResource(file.getBytes()) {
                @Override
                public String getFilename() {
                    return file.getOriginalFilename();
                }
        };

        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("file", resource);

//        file.transferTo(new File("D:/upload", Objects.requireNonNull(file.getOriginalFilename())));

        return "Spring upload success" + " " + restTemplate.postForObject("http://127.0.0.1:5000/uploadFile", params, String.class);
    }
}

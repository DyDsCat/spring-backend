package com.phoenix.springbackend.controller;

import com.phoenix.springbackend.service.AdminService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

    @Resource
    private AdminService adminService;

    @GetMapping("/admin/login")
    public String adminLogin() {
        return adminService.test();
    }
}

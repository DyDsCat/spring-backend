package com.phoenix.springbackend.entity;

import lombok.Data;

@Data
public class Admin {
    private String id;

    private String pwd;
}

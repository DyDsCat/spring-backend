package com.phoenix.springbackend.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("user")
public class User {
    private String id;

    private String pwd;

    private String apiKey;
}

package com.phoenix.springbackend;

import com.phoenix.springbackend.mapper.UserMapper;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.SQLException;

@SpringBootTest
class SpringBackendApplicationTests {

    @Resource
    private UserMapper userMapper;

    @Test
    void contextLoads() {
    }

    @Test
    @Transactional
    public void test() throws SQLException {
//        List<User> userList = userMapper.findAll();
//        userList.forEach(System.out::println);
//        System.out.println(dataSource.getConnection());
        userMapper.selectList(null).forEach(System.out::println);
    }
}
